package com.danit.bank.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class TransferAccountDto {
    private final String number;
    private final double amount;
}
