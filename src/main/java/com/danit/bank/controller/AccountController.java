package com.danit.bank.controller;

import com.danit.bank.dto.TransferAccountDto;
import com.danit.bank.dto.TransferFromAccountToAccountDto;
import com.danit.bank.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AccountController {
    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/deposit")
    public ResponseEntity<?> addToAccount(@RequestBody TransferAccountDto accountDto) {
        accountService.addToAccount(accountDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/withdraw")
    public ResponseEntity<?> withdrawFromAccount(@RequestBody TransferAccountDto accountDto) {
        accountService.withdrawFromAccount(accountDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/transfer")
    public ResponseEntity<?> transferFromAccountToAccount(@RequestBody TransferFromAccountToAccountDto transferData) {
        accountService.transferFromAccountToAccount(transferData);
        return ResponseEntity.ok().build();
    }
}
