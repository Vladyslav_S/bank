package com.danit.bank.service;

import com.danit.bank.model.Account;
import com.danit.bank.model.Customer;

import java.util.List;

public interface CustomerService {
    Customer getCustomerData(Long id);
    List<Customer> getAllCustomersData();
    void createCustomer(Customer customer);
    void changeCustomer(Long id, Customer customer);
    void deleteCustomer(Long id);
    void createCustomerAccount(Long id, Account account);
    void deleteCustomerAccount(Long id, Long accountId);
}
