package com.danit.bank.service;

import com.danit.bank.dto.TransferAccountDto;
import com.danit.bank.dto.TransferFromAccountToAccountDto;
import com.danit.bank.model.Account;
import com.danit.bank.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void addToAccount(TransferAccountDto dto) {
        Account account = accountRepository.findByAccountNumber(dto.getNumber());
        account.setBalance(account.getBalance() + dto.getAmount());
        accountRepository.save(account);
    }

    @Override
    public void withdrawFromAccount(TransferAccountDto dto) {
        Account account = accountRepository.findByAccountNumber(dto.getNumber());
        if (account.getBalance() >= dto.getAmount()) {
            account.setBalance(account.getBalance() - dto.getAmount());
            accountRepository.save(account);
        } else {
            throw new RuntimeException("Not enough money");
        }
    }

    @Override
    public void transferFromAccountToAccount(TransferFromAccountToAccountDto dto) {
        TransferAccountDto from = dto.getFrom();
        String to = dto.getTo();
        Account accountFrom = accountRepository.findByAccountNumber(from.getNumber());
        Account accountTo = accountRepository.findByAccountNumber(to);
        if (accountFrom.getBalance() >= from.getAmount()) {
            accountFrom.setBalance(accountFrom.getBalance() - from.getAmount());
            accountTo.setId((long) (accountTo.getBalance() + from.getAmount()));
            accountRepository.save(accountFrom);
            accountRepository.save(accountTo);
        } else {
            throw new RuntimeException("Not enough money");
        }
    }
}
