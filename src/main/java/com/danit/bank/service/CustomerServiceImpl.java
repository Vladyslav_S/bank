package com.danit.bank.service;

import com.danit.bank.model.Account;
import com.danit.bank.model.Customer;
import com.danit.bank.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer getCustomerData(Long id) {
        return customerRepository.findById(id)
                .orElseThrow();
    }

    @Override
    public List<Customer> getAllCustomersData() {
        return customerRepository.findAll();
    }

    @Override
    public void createCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void changeCustomer(Long id, Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void deleteCustomer(Long id) {
        customerRepository.findById(id)
                .ifPresent(customerRepository::delete);
    }

    @Override
    public void createCustomerAccount(Long id, Account account) {
        Customer customer = customerRepository.findById(id)
                .orElseThrow();
        List<Account> accounts = customer.getAccounts();
        accounts.add(account);
        customer.setAccounts(accounts);
    }

    @Override
    public void deleteCustomerAccount(Long id, Long accountId) {
        Customer customer = customerRepository.findById(id)
                .orElseThrow();
        List<Account> accounts = customer.getAccounts();

        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getId().equals(accountId)) {
                accounts.remove(accounts.get(i));
                break;
            }
        }

        customer.setAccounts(accounts);
        customerRepository.save(customer);
    }
}
