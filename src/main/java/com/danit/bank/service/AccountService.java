package com.danit.bank.service;

import com.danit.bank.dto.TransferAccountDto;
import com.danit.bank.dto.TransferFromAccountToAccountDto;


public interface AccountService {
    void addToAccount(TransferAccountDto dto);
    void withdrawFromAccount(TransferAccountDto dto);
    void transferFromAccountToAccount(TransferFromAccountToAccountDto dto);
}
