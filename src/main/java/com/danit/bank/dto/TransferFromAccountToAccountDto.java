package com.danit.bank.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class TransferFromAccountToAccountDto {
    private final TransferAccountDto from;
    private final String to;
}
