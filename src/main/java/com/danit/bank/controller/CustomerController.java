package com.danit.bank.controller;

import com.danit.bank.model.Account;
import com.danit.bank.model.Customer;
import com.danit.bank.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {
    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/{id}")
    public Customer getCustomerData(@PathVariable("id") Long id) {
        return customerService.getCustomerData(id);
    }

    @GetMapping
    public List<Customer> getAllCustomersData() {
        return customerService.getAllCustomersData();
    }

    @PostMapping
    public ResponseEntity<?> createCustomer(@RequestBody Customer customer) {
        customerService.createCustomer(customer);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> changeCustomer(@PathVariable("id") Long id, @RequestBody Customer customer) {
       customerService.changeCustomer(id, customer);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("id") Long id) {
        customerService.deleteCustomer(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id}/account")
    public ResponseEntity<?> createCustomerAccount(@PathVariable("id") Long id, @RequestBody Account account) {
        customerService.createCustomerAccount(id, account);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}/account/{accountId}")
    public ResponseEntity<?> deleteCustomerAccount(@PathVariable("id") Long id, @PathVariable Long accountId) {
        customerService.deleteCustomerAccount(id, accountId);
        return ResponseEntity.ok().build();
    }
}
